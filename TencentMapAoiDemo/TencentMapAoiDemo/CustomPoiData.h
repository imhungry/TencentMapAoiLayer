//
//  CustomPoiData.h
//  TencentMapAoiDemo
//
//  Created by v_hefang on 2020/8/6.
//  Copyright © 2020 fh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class QMSPoiData;

NS_ASSUME_NONNULL_BEGIN

@interface CustomPoiData : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *tel;
@property (nonatomic, assign) CLLocationCoordinate2D location;
// POI与定位点当前距离
@property (nonatomic, assign) double distance;

- (instancetype)initWithPoiData:(QMSPoiData *)data;
    
@end

NS_ASSUME_NONNULL_END
