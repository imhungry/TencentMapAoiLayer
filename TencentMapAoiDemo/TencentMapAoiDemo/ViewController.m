//
//  ViewController.m
//  TencentMapAoiDemo
//
//  Created by v_hefang on 2020/8/6.
//  Copyright © 2020 fh. All rights reserved.
//

#import "ViewController.h"
#import "PoiInfoView.h"
#import "CustomPoiData.h"

#import <QMapKit/QMapKit.h>
#import <QMapKit/QMSSearchKit.h>

@interface ViewController () <QMapViewDelegate, QMSSearchDelegate>
@property (nonatomic, strong) QMapView *mapView;
@property (nonatomic, strong) QMSSearcher *mapSearcher;
@property (nonatomic, strong) NSString *poiName;
@property (nonatomic, strong) NSString *poiId;
@property (nonatomic, strong) QAOILayer *aoiLayer;
@property (nonatomic, strong) QUserLocation *userLocation;
@property (nonatomic, strong) PoiInfoView *poiInfoView;
@property (nonatomic, strong) QMSPoiData *poiData;
@property (nonatomic, strong) QPointAnnotation *annotation;
@end

@implementation ViewController

#pragma mark - Lazy Load
- (QMSSearcher *)mapSearcher {
    if (_mapSearcher == nil) {
        _mapSearcher = [[QMSSearcher alloc] initWithDelegate:self];
    }
    
    return _mapSearcher;
}

- (PoiInfoView *)poiInfoView {
    if (_poiInfoView == nil) {
        _poiInfoView = [[NSBundle mainBundle] loadNibNamed:@"PoiInfoView" owner:nil options:nil].firstObject;
        _poiInfoView.hidden = YES;
        [self.mapView addSubview:_poiInfoView];
        _poiInfoView.frame = CGRectMake(0, self.mapView.frame.size.height - 100, self.mapView.frame.size.width, 100);
    }
    return _poiInfoView;
}

- (QPointAnnotation *)annotation {
    if (_annotation == nil) {
        _annotation = [[QPointAnnotation alloc] init];
    }
    
    return _annotation;
}


#pragma mark - MapView Delegate
- (void)mapView:(QMapView *)mapView didTapPoi:(QPoiInfo *)poi {
    // 判断点击的是否为同一个POI
    if (_poiName != nil && [_poiName isEqualToString:poi.name]) {
        _poiName = nil;
        [self removeAoiLayer];
        return;
    }
    
    _poiName = poi.name;
    QMSPoiSearchOption *option = [[QMSPoiSearchOption alloc] init];
    option.keyword = poi.name;
    [option setBoundaryByRegionWithCityName:@"北京" autoExtend:YES center:poi.coordinate];
    [self.mapSearcher searchWithPoiSearchOption:option];
}

- (void)mapView:(QMapView *)mapView didUpdateUserLocation:(QUserLocation *)userLocation fromHeading:(BOOL)fromHeading {
    _userLocation = userLocation;
}

- (QAnnotationView *)mapView:(QMapView *)mapView viewForAnnotation:(id<QAnnotation>)annotation {
    static NSString *annotationId = @"id";
    
    if ([annotation isKindOfClass:[QPointAnnotation class]]) {
        
        QPinAnnotationView *pinView = (QPinAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:annotationId];
        if (pinView == nil) {
            pinView = [[QPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationId];
        }
        return pinView;
    }
    
    return nil;
}


#pragma mark - Searcher Delegate
- (void)searchWithPoiSearchOption:(QMSPoiSearchOption *)poiSearchOption didReceiveResult:(QMSPoiSearchResult *)poiSearchResult {
    
    // 解析出POI的id
    if (poiSearchResult.dataArray.count > 0) {
        QMSPoiData *data = poiSearchResult.dataArray.firstObject;
        if (data.id_ != nil && data.id_.length > 0) {
            _poiData = data;
            _poiId = data.id_;
            [self createAoiLayer];
        }
    }
}

#pragma mark - AOI Layer
- (void)createAoiLayer {
    [self removeAoiLayer];
    
    _aoiLayer = [[QAOILayer alloc] initWithUID:_poiId];
    
    __weak typeof(self) weakSelf = self;
    [self.mapView addAOILayer:_aoiLayer callback:^(BOOL success) {
        if (success) {
            NSLog(@"添加成功");
            
            // 显示PoiInfoView
            CustomPoiData *customData = [[CustomPoiData alloc] initWithPoiData:weakSelf.poiData];
            customData.distance = [weakSelf calculateDistanceWithLocation:customData.location];
            weakSelf.poiInfoView.poiData = customData;
            weakSelf.poiInfoView.hidden = NO;
            
            // 添加Annotation
            weakSelf.annotation.coordinate = customData.location;
            if (![weakSelf.mapView.annotations containsObject:weakSelf.annotation]) {
                [weakSelf.mapView addAnnotation:weakSelf.annotation];
            }
            
            // 调整视野
            [weakSelf.mapView setCenterCoordinate:customData.location animated:YES];
            [weakSelf.mapView setZoomLevel:17.0 animated:YES];
        } else {
            NSLog(@"添加失败");
            weakSelf.poiInfoView.hidden = YES;
            [weakSelf.mapView removeAnnotation:weakSelf.annotation];
        }
    }];
}

- (void)removeAoiLayer {
    if (_aoiLayer) {
        [self.mapView removeAOILayer:_aoiLayer];
        self.poiInfoView.hidden = YES;
        [self.mapView removeAnnotation:self.annotation];
    }
}

#pragma mark - Calculate Distance
- (double)calculateDistanceWithLocation:(CLLocationCoordinate2D)location {
    return QMetersBetweenCoordinates(location, self.userLocation.location.coordinate);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView = [[QMapView alloc] init];
    self.mapView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    self.mapView.delegate = self;
    [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(40.040219,116.273348)];
    [self.mapView setZoomLevel:17.0 animated:YES];
    [self.view addSubview:self.mapView];
    
    [self.mapView setShowsUserLocation:YES];
}




@end
