//
//  CustomPoiData.m
//  TencentMapAoiDemo
//
//  Created by v_hefang on 2020/8/6.
//  Copyright © 2020 fh. All rights reserved.
//

#import "CustomPoiData.h"
#import <QMapKit/QMSSearchKit.h>

@implementation CustomPoiData

- (instancetype)initWithPoiData:(QMSPoiData *)data {
    if (self = [super init]) {
        _name = data.title;
        _address = data.address;
        _category = data.category;
        _tel = data.tel;
        _location = data.location;
    }
    
    return self;
}

@end
