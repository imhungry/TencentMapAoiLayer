//
//  AppDelegate.h
//  TencentMapAoiDemo
//
//  Created by v_hefang on 2020/8/6.
//  Copyright © 2020 fh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

