//
//  PoiInfoView.h
//  TencentMapAoiDemo
//
//  Created by v_hefang on 2020/8/6.
//  Copyright © 2020 fh. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomPoiData;

NS_ASSUME_NONNULL_BEGIN

@interface PoiInfoView : UIView

@property (nonatomic, strong) CustomPoiData *poiData;

@end

NS_ASSUME_NONNULL_END
