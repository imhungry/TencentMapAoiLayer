//
//  AppDelegate.m
//  TencentMapAoiDemo
//
//  Created by v_hefang on 2020/8/6.
//  Copyright © 2020 fh. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

#import <QMapKit/QMapKit.h>
#import <QMapKit/QMSSearchKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [QMapServices sharedServices].APIKey = @"6NCBZ-VPCCU-ULWV2-BIA7K-QNBMK-XEFTC";
    [QMSSearchServices sharedServices].apiKey = @"6NCBZ-VPCCU-ULWV2-BIA7K-QNBMK-XEFTC";

    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = [[ViewController alloc] init];
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
