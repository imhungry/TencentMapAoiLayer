//
//  PoiInfoView.m
//  TencentMapAoiDemo
//
//  Created by v_hefang on 2020/8/6.
//  Copyright © 2020 fh. All rights reserved.
//

#import "PoiInfoView.h"
#import "CustomPoiData.h"

@interface PoiInfoView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *telLabel;
@end

@implementation PoiInfoView

- (void)setPoiData:(CustomPoiData *)poiData {
    _poiData = poiData;
    _titleLabel.text = _poiData.name;
    _distanceLabel.text = [NSString stringWithFormat:@"%.2f公里", (poiData.distance / 1000)];
    _addressLabel.text = _poiData.address;
    _categoryLabel.text = _poiData.category;
    if (_poiData.tel && _poiData.tel.length > 3) {
        _telLabel.text = [NSString stringWithFormat:@"电话：%@", _poiData.tel];
    } else {
        _telLabel.text = @"电话：暂无";
    }
    
    [_titleLabel sizeToFit];
    [_distanceLabel sizeToFit];
    [_addressLabel sizeToFit];
    [_categoryLabel sizeToFit];
    [_telLabel sizeToFit];
}

@end
