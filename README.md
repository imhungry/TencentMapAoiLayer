# TencentMapAoiLayer

#### 介绍
腾讯地图SDK在4.3.8版本中增加了AOILayer功能，该功能既是腾讯地图APP中，点击任意建筑图标，会出现该建筑物的轮廓以及建筑的相关信息功能。

本次腾讯地图SDK将该功能开源，虽然比腾讯地图APP中的功能略有欠缺，但是AOI功能还是比较实用的。

#### 使用SDK
[腾讯地图SDK 4.3.9](https://lbs.qq.com/mobile/iOSMapSDK/mapDownload/download3D)



